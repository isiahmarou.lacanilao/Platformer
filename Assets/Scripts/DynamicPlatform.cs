using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;

public class DynamicPlatform : MonoBehaviour
{
    public float _upOffset, _downOffset;
    private float upPos, downPos;
    private Rigidbody2D rb;
    public float upSpeed = 20, downSpeed = 5;
    private bool isAtPos = false;
    private Vector3 startingPos;
    IEnumerator Start()
    {
        yield return new WaitForSeconds(1f);
        Vector3 pos = transform.position;
        rb = GetComponent<Rigidbody2D>();
        upPos = pos.y + _upOffset;
        downPos = pos.y - _downOffset;
        startingPos = transform.position;
        StartCoroutine(PlatformMovementCoroutine());
    }

    #region MathF Implementation

    private void Update()
    {
        // float t = Mathf.SmoothStep(0, 1, Mathf.Sin(Time.time)) * 4f - 2f;
        //
        // transform.position = new Vector3(startingPos.x, startingPos.y + t, startingPos.z);
    }
    
    #endregion
    
    #region ASYNC Implementation

    
    async void PlatformMovement()
    {
        await Task.Delay(500);
        while (this.enabled)
        {
             await GoUp();
             await Task.Delay(1000);
             await GoDown();
             await Task.Delay(1000);
        }
    }

    async Task GoUp()
    {
        isAtPos = false;
        while (!isAtPos)
        {
            rb.velocity = new Vector2(rb.velocity.x, upSpeed);
            isAtPos = Mathf.Approximately(transform.position.y, upPos) || transform.position.y > upPos;
            if(isAtPos) break;
            await Task.Yield();
        }
        Debug.Log("STOP");
        rb.velocity = new Vector2(rb.velocity.x, 0);
    }

    async Task GoDown()
    {
        isAtPos = false;
        while (!isAtPos)
        {
            rb.velocity = new Vector2(rb.velocity.x, -downSpeed);
            isAtPos = Mathf.Approximately(transform.position.y, downPos) || transform.position.y < downPos;
            if(isAtPos) break;
            await Task.Yield();
        }
        Debug.Log("STOP");
        rb.velocity = new Vector2(rb.velocity.x, 0);
    }
    # endregion

    #region Coroutine Implementation
    
    IEnumerator PlatformMovementCoroutine()
    {
        yield return new WaitForSeconds(1f);
        

        while (this.enabled)
        {
            //yield return UpCoroutine();
            yield return UpCoroutine();
            yield return new WaitForSeconds(1f);
            yield return DownCoroutine();
            yield return new WaitForSeconds(2f);
        }
    }
    
    IEnumerator UpCoroutine()
    {
        isAtPos = false;
        while (!isAtPos)
        {
            rb.velocity = new Vector2(rb.velocity.x, upSpeed);
            isAtPos = Mathf.Approximately(transform.position.y, upPos) || transform.position.y > upPos;
            if(isAtPos) break;
            yield return new WaitForFixedUpdate();
        }
        rb.velocity = new Vector2(rb.velocity.x, 0);
    }

    IEnumerator DownCoroutine()
    {
        isAtPos = false;
        while (!isAtPos)
        {
            rb.velocity = new Vector2(rb.velocity.x, -downSpeed);
            isAtPos = Mathf.Approximately(transform.position.y, downPos) || transform.position.y < downPos;
            if(isAtPos) break;
            yield return new WaitForFixedUpdate();
        }
        rb.velocity = new Vector2(rb.velocity.x, 0);
    }
    
    #endregion
}

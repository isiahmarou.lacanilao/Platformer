using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    private Transform player;
    public Vector3 offset = new Vector3(0, 0, -10);

    [Range(1,10)]
    public float smoothFactor = 5;
    void Start()
    {
        player = FindObjectOfType<PlayerController>().transform;
    }

    void FixedUpdate()
    {
        FollowPlayer();
    }

    void FollowPlayer()
    {
        Vector3 targetPos = player.position + offset;
        Vector3 smoothPos = Vector3.Lerp(transform.position, targetPos, smoothFactor * Time.deltaTime);
        transform.position = smoothPos;
    }
}

using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private Rigidbody2D rb;
    private CollisionDetector col;
    private Jump _jumpComponent;

    [Header("Bools")] 
    private bool isGrabWall, canMove, wallJumped, wallSlide, isDashing, hasDashed, groundTouch;
    
    
    [Header("Movement Properties")]
    public float speed = 10;
    public float jumpForce = 50;
    public float slideSpeed = 5;
    public float wallJumpForce = 10;
    public float dashSpeed = 20;
    public float wallJumpLerp = 10;

    
    
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        col = GetComponent<CollisionDetector>();
        _jumpComponent = GetComponent<Jump>();
        canMove = true;
    }

    // Update is called once per frame
    void Update()
    {
        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");
        float xRaw = Input.GetAxisRaw("Horizontal");
        float yRaw = Input.GetAxisRaw("Vertical");
        Vector2 dir = new Vector2(x, y);

        Walk(dir);
        isGrabWall = col.onWall && Input.GetButton("Fire3") && canMove;
        if (isGrabWall) wallSlide = false;

        if (Input.GetButtonUp("Fire3") || !col.onWall || !canMove)
        {
            isGrabWall = false;
            wallSlide = false;
        }

        if (col.isGrounded && !isDashing)
        {
            wallJumped = false;
            _jumpComponent.enabled = false;
        }

        if (isGrabWall && !isDashing)
        {
            rb.gravityScale = 0;
            if (x > .2f || x < -.2f)
            {
                rb.velocity = new Vector2(rb.velocity.x, 0);
                float speedMod = y > 0 ? 0.5f : 1f;

                rb.velocity = new Vector2(rb.velocity.x, y * (speed * speedMod));
            }
        }
        else
        {
            rb.gravityScale = 3f;
        }

        if (col.onWall && !col.isGrounded)
        {
            if (x != 0 && !isGrabWall)
            {
                wallSlide = true;
                WallSlide();
            }
        }

        if (!col.onWall || col.isGrounded) wallSlide = false;


        if (Input.GetButtonDown("Jump"))
        {
            if(col.isGrounded) Jump(Vector2.up);
            if(col.onWall && !col.isGrounded) WallJump(); 
        }
        
        if (Input.GetButtonDown("Fire1") && !hasDashed)
        {
            if(xRaw != 0 || yRaw != 0)
                Dash(xRaw, yRaw);
        }

        if (col.isGrounded && !groundTouch)
        {
            GroundTouch();
            groundTouch = true;
        }

        if(!col.isGrounded && groundTouch)
        {
            groundTouch = false;
        }
    }

    void GroundTouch()
    {
        hasDashed = false;
        isDashing = false;
    }
    
    private void Dash(float x, float y)
    {
        hasDashed = true;

        rb.velocity = Vector2.zero;
        Vector2 dir = new Vector2(x, y);

        rb.velocity += dir.normalized * dashSpeed;
        StartCoroutine(DashWait());
    }
    
    
    IEnumerator DashWait()
    {
        StartCoroutine(GroundDash());
        //DOVirtual.Float(14, 0, .8f, RigidbodyDrag);
        
        rb.gravityScale = 0;
        _jumpComponent.enabled = false;
        wallJumped = true;
        isDashing = true;

        yield return new WaitForSeconds(.3f);

        rb.gravityScale = 3;
        _jumpComponent.enabled = true;
        wallJumped = false;
        isDashing = false;
    }
    
    
    IEnumerator GroundDash()
    {
        yield return new WaitForSeconds(.15f);
        if (col.isGrounded)
            hasDashed = false;
    }
    
    void Walk(Vector2 dir)
    {
        if (!canMove)
            return;

        if (isGrabWall)
            return;

        rb.velocity = !wallJumped ? new Vector2(dir.x * speed, rb.velocity.y) : Vector2.Lerp(rb.velocity, (new Vector2(dir.x * speed, rb.velocity.y)), wallJumpLerp * Time.deltaTime);
    }
    
    private void WallJump()
    {

        StopCoroutine(DisableMovement(0));
        StartCoroutine(DisableMovement(.1f));

        Vector2 wallDir = col.onRightWall ? Vector2.left : Vector2.right;

        Jump((Vector2.up / 1.5f + wallDir / 1.5f));

        wallJumped = true;
    }

    IEnumerator DisableMovement(float time)
    {
        canMove = false;
        yield return new WaitForSeconds(time);
        canMove = true;
    }
    
    private void Jump(Vector2 dir)
    {
        rb.velocity = new Vector2(rb.velocity.x, 0);
        rb.velocity += dir * jumpForce;
    }

    private void WallSlide()
    {

        if (!canMove)
            return;

        bool pushingWall = (rb.velocity.x > 0 && col.onRightWall) || (rb.velocity.x < 0 && col.onLeftWall);
        
        float push = pushingWall ? 0 : rb.velocity.x;

        rb.velocity = new Vector2(push, -slideSpeed);
    }
    
    void RigidbodyDrag(float x)
    {
        rb.drag = x;
    }
}

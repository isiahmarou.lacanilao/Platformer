using System;
using UnityEngine;
using Random = UnityEngine.Random;

public struct ControllerInput
{
    public float X, Y;
    public int RawX, RawY;
}

public class PlayerController : MonoBehaviour
{
    [Header("Components")]
    private Rigidbody2D rb;
    private CollisionDetector col;
    private ControllerInput _inputs;
    private Animator animator;
    private SpriteRenderer spriteRenderer;
    private GhostTrail ghostTrail;
    [SerializeField] private ParticleSystem groundParticle;
    [SerializeField] private ParticleSystem dashParticle;

    [Header("Movement Bools")]
    private bool facingLeft, isPushingLeftWall, isPushingRightWall, canMove;

    [Header("Movement Values")] 
    [SerializeField] private float movementSpeed = 4;
    [SerializeField] private float acceleration = 2;
    [SerializeField] private float currentMoveLerpSpeed = 100;

    [Header("Jump Values")] 
    [SerializeField] private float jumpForce = 15;
    [SerializeField] private float fallMult = 7;
    [SerializeField] private float jumpVelFallOff = 8;
    [SerializeField] private float coyoteTime = 0.2f;
    [SerializeField] private bool canDoubleJump = false;
    
    private bool hasJumped, hasDoubleJumped, hasLanded;
    private float timeLastJumped, prevVelocityY;
    

    [Header("Dash Values")] 
    [SerializeField] private float dashSpeed = 15f;
    [SerializeField] private float dashDuration = 1;
    [SerializeField] private float dashCooldown = 0.2f;
    
    private bool hasDashed, isDashing;
    private Vector2 dashDir;
    private float timeLastDashed, defaultGravityScale;
    
    [Header("Attack Values")]
    //[SerializeField] private int Damage = 0;
    [SerializeField] private LayerMask attackLayers;
    
    private bool isAttacking;
    private bool hasAttacked;
    
    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        col = GetComponent<CollisionDetector>();
        canMove = true;
        defaultGravityScale = rb.gravityScale;
        spriteRenderer = GetComponent<SpriteRenderer>();
        ghostTrail = GetComponent<GhostTrail>();
        ghostTrail.enabled = false;
    }

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    private void Update()
    {
        InputHandler();
        Walk();
        Jump();
        Dash();
        Attack();
    }

    private void LateUpdate()
    {
        UpdateValues();
    }

    private void InputHandler()
    {
        if(isWallJumping) return;
        _inputs.RawX = (int) Input.GetAxisRaw("Horizontal");
        _inputs.RawY = (int) Input.GetAxisRaw("Vertical");
        _inputs.X = Input.GetAxis("Horizontal");
        _inputs.Y = Input.GetAxis("Vertical");
        bool isPrevLeft = facingLeft;
        facingLeft = _inputs.RawX != 1 && (_inputs.RawX == -1 || facingLeft);
        
        // if change of orientation
        if(isPrevLeft != facingLeft) groundParticle.Play();
            
        spriteRenderer.flipX = facingLeft;

        isPushingLeftWall = col.onLeftWall && _inputs.X < 0;
        isPushingRightWall = col.onRightWall && _inputs.X > 0;
    }

    private void UpdateValues()
    {
        if (!hasLanded && col.isGrounded)
        {
            hasLanded = true;
            hasDashed = false;
            hasJumped = false;
            currentMoveLerpSpeed = 100;
            prevVelocityY = 0;
            isWallJumping = false;
        }
        else if (hasLanded && !col.isGrounded)
        {
            hasLanded = false;
            timeLastJumped = Time.time;
        }
    }
    
    void Walk()
    {
        if(!canMove) return;
        if(isAttacking) return;
        if(isWallJumping) return;

        float newAcceleration = col.isGrounded ? acceleration : acceleration / 2f;

        if (Input.GetKey(KeyCode.A))
        {
            if (rb.velocity.x > 0) _inputs.X = 0;
            _inputs.X = Mathf.MoveTowards(_inputs.X, -1, newAcceleration * Time.deltaTime);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            if (rb.velocity.x < 0) _inputs.X = 0;
            _inputs.X = Mathf.MoveTowards(_inputs.X, 1, newAcceleration * Time.deltaTime);
        }
        else
        {
            _inputs.X = Mathf.MoveTowards(_inputs.X, 0, newAcceleration * 2 * Time.deltaTime);
        }

        Vector3 newVelocity = new Vector3(_inputs.X * movementSpeed, rb.velocity.y);
        rb.velocity = Vector2.MoveTowards(rb.velocity, newVelocity, currentMoveLerpSpeed * Time.deltaTime);
        
        if(!hasLanded) return;
        if(isDashing) return;

        animator.Play(Mathf.Approximately(rb.velocity.x, 0) ? "Idle" : "Run");
    }

    private bool isWallJumping = false;
    private void Jump()
    {
        if(isDashing) return;
        if(isAttacking) return;
        
        if(Input.GetButtonDown("Jump"))
        {
            if (col.isGrounded || Time.time < timeLastJumped + coyoteTime || canDoubleJump && !hasDoubleJumped)
            {
                if (!hasJumped || hasJumped && !hasDoubleJumped)
                {
                    prevVelocityY = rb.velocity.y;
                    rb.velocity = new Vector2(rb.velocity.x, jumpForce);
                    hasDoubleJumped = hasJumped;
                    hasJumped = true;
                    groundParticle.Play();
                }
            }

            if (hasJumped && !hasLanded && (isPushingLeftWall || isPushingRightWall))
            {
                rb.velocity =  new Vector2(col.wallSide * - 1f * (jumpForce/2f), jumpForce);
                hasJumped = true;
                isWallJumping = true;
            }
        }

        if (rb.velocity.y < jumpVelFallOff || rb.velocity.y > 0 && !Input.GetButton("Jump"))
        {
            rb.velocity += Vector2.up * (fallMult * Physics2D.gravity.y * Time.deltaTime);
        }
        
        if(hasLanded && col.isGrounded) return;
        if(Mathf.Approximately(rb.velocity.y, 0)) return;
        else if(rb.velocity.y < 0) animator.Play("Fall");
        else if(rb.velocity.y > 0) animator.Play("Jump");
    }

    private void Dash()
    {
        if(isAttacking) return;
        if (Input.GetButtonDown("Fire3") && !isDashing && (Time.time >= timeLastDashed + dashCooldown) && !hasDashed)
        {
            dashDir = new Vector2(_inputs.RawX, _inputs.RawY).normalized;

            if (dashDir == Vector2.zero) dashDir = facingLeft ? Vector2.left : Vector2.right;

            isDashing = true;
            hasDashed = true;
            timeLastDashed = Time.time;
            rb.gravityScale = 0;
            ghostTrail.enabled = true;
            dashParticle.transform.position = facingLeft
                ? (Vector2) transform.position + col.leftOffset
                : (Vector2) transform.position + col.rightOffset;
            dashParticle.Play();
        }

        if (!isDashing) return;
        rb.velocity = dashDir * dashSpeed;

        // end of dash
        if (!(Time.time >= timeLastDashed + dashDuration)) return;
        
        isDashing = false;
        ghostTrail.enabled = false;
        float yValue = Mathf.Clamp(rb.velocity.y, -10, 10);
        rb.velocity = new Vector2(rb.velocity.x, yValue);
        rb.gravityScale = defaultGravityScale;

        if (hasLanded) hasDashed = false;
    }

    private int attackComboNum = 0;
    private float attackComboInterval = 1f;
    private float timeLastAttack;
    private bool hasSecondAtkBuffer;
    private void Attack()
    {
        float currentAnimPercent = animator.GetCurrentAnimatorStateInfo(0).normalizedTime;
        
        if(Time.time > timeLastAttack + attackComboInterval)
        {
            hasSecondAtkBuffer = false;
            isAttacking = false;
            hasAttacked = false;
        }
        
        if (Input.GetButton("Fire2"))
        {
            if (!isAttacking)
            {
                if (!hasAttacked)
                {
                    animator.Play("Attack1");

                    isAttacking = true;
                    hasAttacked = true;
                    timeLastAttack = Time.time;
                }
                else if(hasAttacked && Time.time <= timeLastAttack + attackComboInterval)
                {
                    animator.Play("Attack2");
                    isAttacking = true;
                    hasAttacked = false;
                }
            }
            else if(isAttacking && (currentAnimPercent > 0.5f) && currentAnimPercent < 1.0f && !hasSecondAtkBuffer)
            {
                Debug.Log("has buffer????");
                hasSecondAtkBuffer = true;
            }
        }
        
        if (isAttacking && animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 1)
        {
            if (hasSecondAtkBuffer)
            {
                animator.Play("Attack2");
                isAttacking = true;
                hasAttacked = false;
                hasSecondAtkBuffer = false;
            }
            else
            {
                isAttacking = false;
                animator.Play("Idle");
            }
        }
            
    }

    public void DamageOpponents()
    {
        Collider2D[] collider2Ds = facingLeft
            ? col.GetCollidingGameObjectsOnLeft(attackLayers)
            : col.GetCollidingGameObjectsOnRight(attackLayers);
        foreach (Collider2D collider in collider2Ds)
        {
            // damage
            
        }
    }
}

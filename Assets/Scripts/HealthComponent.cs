using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthComponent : MonoBehaviour
{
    [SerializeField] private int maxHealth = 5;
    public int currentHealth { get; private set; }
    public int MaxHealth => maxHealth; 
    public Action<GameObject> OnDamaged, OnDeath, OnHeal, OnMaxHealth;
    void Start()
    {
        currentHealth = maxHealth;
    }

    public void SetMaxHealth(int newHP)
    {
        maxHealth = newHP;
    }

    public void DamageHealth(int damageValue)
    {
        currentHealth -= damageValue;

        if (currentHealth <= 0)
        {
            currentHealth = 0;
            OnDeath?.Invoke(this.gameObject);
        }
        else OnDamaged?.Invoke(this.gameObject);
    }

    public void HealHealth(int healValue)
    {
        currentHealth += healValue;

        if (currentHealth >= maxHealth)
        {
            currentHealth = maxHealth;
            OnMaxHealth?.Invoke(this.gameObject);
        }
        else OnHeal?.Invoke(this.gameObject);
    }
}
